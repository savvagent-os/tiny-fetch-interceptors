class FetchClient {
  constructor(interceptors = [], _fetch) {
    const f = typeof fetch === 'function' && typeof window !== 'undefined' ? fetch.bind(window) : typeof global !== 'undefined' ? fetch.bind(global) : fetch;
    this.interceptors = [ ...interceptors];
    this.requestMap = new Map();
    this.requestCache = new Map();
    this.fetch = f;
  }

  clear() {
    this.interceptors = [];
  }

  interceptor(...args) {
    let promise = Promise.resolve(args);

    this.interceptors.forEach(({ request, requestError }) => {
      if (request || requestError) {
        promise = promise.then(_args => request(..._args), requestError);
      }
    });

    // Register fetch call
    promise = promise.then(_args => {
      const [url, config ] = _args;
      return this.fetch(url, config);
    });

    // Register response interceptors
    this.interceptors.forEach(({ response, responseError }) => {
      if (response || responseError) {
        promise = promise.then(response, responseError);
      }
    });

    return promise;
}

  register(_interceptor, pos) {
    if (Array.isArray(_interceptor)) this.interceptors = [ ...this.interceptors, ..._interceptor];
    else {
      const existing = Boolean(this.interceptors.find(i => i.id === _interceptor.id));
      if (pos !== 'undefined' && !existing) {
        this.interceptors.splice(pos, 0, _interceptor);
      } else if (!existing) this.interceptors = [ ...this.interceptors, _interceptor];
    }
  }

  request(...args) {
    const [ url, config ] = args;
    const conf = config || {};
    const loading = this.requestMap.get(url);
    const cached = this.requestCache.has(url);
    if (loading) return loading;
    if (cached) return this.requestCache.get(url).response;
    const response = this.interceptor(...args)
      .finally(() => {
        this.requestMap.delete(url);
        const cache = this.requestCache.get(url);
        const { ttl } = cache;
        setTimeout(() => this.requestCache.delete(url), ttl);
    });
    this.requestMap.set(url, response);
    this.requestCache.set(url, { ttl: conf.ttl || 0, response});
    return response;
  }

  unregister(interceptorId) {
    this.interceptors = this.interceptors.filter(i => i.id !== interceptorId);
  }

  get interceptors() {
    return this._interceptors;
  }

  set interceptors(val) {
    this._interceptors = val;
  }
}

export { FetchClient };
