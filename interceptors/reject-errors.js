import isJson from './isJson.js';

export const rejectErrors = {
  response(response) {
    if (isJson(response)) return response;
    if (!response.ok) throw response;
    return response;
  },
  id: 'TINY_REJECT_ERRORS'
};
