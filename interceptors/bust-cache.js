import TinyUri from 'tiny-uri';

export const bustCache = {
  request(url, config = {}) {
    if (config && config.bustCache) {
      const u = new TinyUri(url);
      u.query.add({rn: new Date().getTime().toString()});
      url = u.toString();
    }

    return [url, config];
  },
  id: 'TINY_BUST_CACHE'
};
