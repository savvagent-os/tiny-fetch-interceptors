import isJson from './isJson';

export const jsonRequest = {
  request(url, config = {}) {
    const headers = {Accept: 'application/json', 'Content-Type': 'application/json'};
    if (typeof config.headers === 'object') Object.assign(config.headers, headers);
    else Object.assign(config, {headers});
    if (config.body && !isJson(config.body)) config.body = JSON.stringify(config.body)

    return [url, config];
  },
  id: 'TINY_JSON_REQUEST'
};
