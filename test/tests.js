import fetch from 'node-fetch';

global.fetch = fetch;
import './FetchClient.spec';
import './interceptors.spec';