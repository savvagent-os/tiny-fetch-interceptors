import {expect} from 'chai';
import {FetchClient} from '../src/FetchClient'
import fetchMock from 'fetch-mock';
import { jsonRequest } from '../interceptors/json-request'
import { jsonResponse } from '../interceptors/json-response'
import mockEmployees from './mockEmployees';

describe('FetchClient class', () => {
  let fetchClient;
  const url = 'http://dummy.restapiexample.com/api/v1/employees';

  beforeEach(() => {
    fetchClient = new FetchClient();
    fetchMock.get(url, mockEmployees);
  });

  afterEach(() => {
    fetchMock.resetHistory();
    fetchMock.restore();
    fetchClient = null;
  });

  it('should have certain methods and properties when instantiated', () => {
    expect(fetchClient).to.be.instanceof(FetchClient);
    expect(fetchClient.clear).to.be.a('function');
    expect(fetchClient.request).to.be.a('function');
    expect(fetchClient.register).to.be.a('function');
    expect(fetchClient.unregister).to.be.a('function');
    expect(fetchClient.interceptors).to.be.an('array');
  });

  it('should support creating multiple instances', () => {
    let fetchClient1 = new FetchClient();
    expect(fetchClient).to.not.be.equal(fetchClient1);
  });

  it('should support instantiation with an array of interceptors', function() {
    const arr = [jsonRequest, jsonResponse];
    fetchClient = new FetchClient(arr);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors).to.have.length(2);
    expect(interceptors[0].id).to.be.equal('TINY_JSON_REQUEST');
    expect(interceptors[1].id).to.be.equal('TINY_JSON_RESPONSE');
  });

  it('should register a response interceptor', function() {
    fetchClient.register(jsonResponse);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors[0].id).to.be.equal('TINY_JSON_RESPONSE');
  });

   it('should register an interceptor only once', function() {
    fetchClient.register(jsonResponse);
    fetchClient.register(jsonResponse);
    fetchClient.register(jsonResponse);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors).to.have.length(1);
    expect(interceptors[0].id).to.be.equal('TINY_JSON_RESPONSE');
  });

  it('should register an array of interceptors', function() {
    const arr = [jsonRequest, jsonResponse];
    fetchClient.register(arr);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors).to.have.length(2);
    expect(interceptors[0].id).to.be.equal('TINY_JSON_REQUEST');
    expect(interceptors[1].id).to.be.equal('TINY_JSON_RESPONSE');
  });

  it('should register a response interceptor in a specific location', function() {
    fetchClient.register(jsonResponse);
    fetchClient.register(jsonRequest, 0);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors).to.have.length(2);
    expect(interceptors[0].id).to.be.equal('TINY_JSON_REQUEST');
  });

  it(`should support clearing interceptors`, () => {
    fetchClient.register(jsonResponse);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors[0].id).to.be.equal('TINY_JSON_RESPONSE');
    fetchClient.clear();
    interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors).to.have.length(0);
  });

  it(`should support unregistering an interceptor`, () => {
    fetchClient.register(jsonResponse);
    let interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors[0].id).to.be.equal('TINY_JSON_RESPONSE');
    fetchClient.unregister('TINY_JSON_RESPONSE');
    interceptors = fetchClient.interceptors;
    expect(interceptors).to.be.an('array');
    expect(interceptors).to.have.length(0);
  });

  it(`should dedupe requests`, async function() {
    fetchClient = new FetchClient([jsonRequest, jsonResponse]);
    expect(fetchClient.requestCache.size).to.equal(0);
    const resp1 = await fetchClient.request(url);
    expect(fetchClient.requestCache.size).to.equal(1);
    const resp2 = await fetchClient.request(url);
    expect(fetchClient.requestCache.size).to.equal(1);
    const resp3 = await fetchClient.request(url);
    expect(fetchClient.requestCache.size).to.equal(1);
    const resp4 = await fetchClient.request(url);
    expect(fetchClient.requestCache.size).to.equal(1);
    expect(resp2).to.equal(resp1);
    expect(resp3).to.equal(resp1);
    expect(resp4).to.equal(resp1);
  });

  it(`should consildate concurrent requests`, async function() {
    fetchClient = new FetchClient([jsonRequest, jsonResponse]);
    expect(fetchClient.requestMap.size).to.equal(0);
    const req1 = fetchClient.request(url);
    expect(fetchClient.requestMap.size).to.equal(1);
    const req2 = fetchClient.request(url);
    expect(fetchClient.requestMap.size).to.equal(1);
    const req3 = fetchClient.request(url);
    expect(fetchClient.requestMap.size).to.equal(1);
    const req4 = fetchClient.request(url);
    expect(fetchClient.requestMap.size).to.equal(1);

    const [ one, two, three, four ] = await Promise.all([ req1, req2, req3, req4 ]);
    expect(two).to.equal(one);
    expect(three).to.equal(one);
    expect(four).to.equal(one);

    expect(fetchClient.requestMap.size).to.equal(0);

  });
});