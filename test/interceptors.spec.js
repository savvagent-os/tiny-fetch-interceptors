import {expect} from 'chai';
import { bustCache } from '../interceptors/bust-cache';
import {FetchClient} from '../src/FetchClient'
import fetchMock from 'fetch-mock';
import { jsonRequest } from '../interceptors/json-request'
import { jsonResponse } from '../interceptors/json-response'
import { rejectErrors } from '../interceptors/reject-errors';
import mockEmployees from './mockEmployees';

describe('standard interceptors', () => {
  let fetchClient, sandbox;
  const url = 'http://dummy.restapiexample.com/api/v1/employees';

  beforeEach(() => {
    fetchClient = new FetchClient();
    fetchMock.get(`begin:${url}`, mockEmployees);
    fetchMock.post(`begin:${url}`, mockEmployees);
  });

  afterEach(() => {
    fetchMock.resetHistory();
    fetchMock.restore();
    fetchClient = null;
  });

  it(`should register some interceptors upon invocation`, function() {
    fetchClient = new FetchClient([jsonRequest]);
    expect(fetchClient.interceptors).to.be.an('array');
    expect(fetchClient.interceptors).to.have.length(1);
  });

  it('should make a json request', () => {
    fetchClient = new FetchClient([jsonRequest]);

    return fetchClient.request(url)
      .then((response) => {
        expect(response.status).to.equal(200);
      })
      .catch((err) => expect(err).to.not.exist);
  });

  it('should stringify the body of json request', () => {
    fetchClient = new FetchClient([jsonRequest]);

    return fetchClient.request(url, { method: 'POST', data: { name: 'foo'}})
      .then((response) => {
        expect(response.status).to.equal(200);
      })
      .catch((err) => expect(err).to.not.exist);
  });

  it('should make a json request and get a json response', () => {
    fetchClient = new FetchClient([jsonRequest, jsonResponse]);

    return fetchClient.request(url)
      .then((response) => {
        expect(response.status).to.equal('success');
      })
      .catch((err) => expect(err).to.not.exist);
  });

  describe(`bustCache interceptor`, function() {
    it(`should add a random number string to the request url`, async function() {
      fetchClient = new FetchClient([bustCache]);
      const response = await fetchClient.request(url, { bustCache: true});
      expect(response.url).to.contain('?rn=');
    });
  });

});
